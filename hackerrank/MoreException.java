package hackerrank;

import java.util.*;

//Write your code here
class CalculatorException {

    public int power(int n, int p) throws Exception{
        if(n<0 || p<0){
            throw new Exception("n and p should be non-negative");
        }
        int power = 1;

        for(int i = 0; i<p; i++){
            power*=n;
        }

        return power;
    }
}

class MoreException{

    public static void main(String[] args) {
    
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        while (t-- > 0) {
        
            int n = in.nextInt();
            int p = in.nextInt();
            CalculatorException myCalculatorException = new CalculatorException();
            try {
                int ans = myCalculatorException.power(n, p);
                System.out.println(ans);
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        in.close();
    }
}

