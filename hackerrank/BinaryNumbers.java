package hackerrank;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class BinaryNumbers {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        countOne(n);
        
        scanner.close();
    }

    private static void countOne(int n){

        int count = 0, r = 0, storeCount = 0;

        while(n > 0){

            r = n % 2;
            n /= 2;

            if(r==1){

                count++;

                if(count > storeCount){
                    storeCount = count;
                }

            }else{
                count = 0;
            }
        }

        System.out.println(storeCount);

    }
}

