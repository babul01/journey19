package hackerrank;

import java.io.*;
import java.util.*;

public class LetsReview {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */

            Scanner in = new Scanner(System.in);

            int n = in.nextInt();

            for(int i = 0; i<n; i++){

                    String s = in.next();
                    char[] cs = s.toCharArray();

                    for(int j = 0; j<cs.length; j++){
                        if((j/2)*2==j){
                            System.out.print(cs[j]);
                        }
                    }

                    System.out.print(" ");
                    
                    for(int j = 0; j<cs.length; j++){
                        if((j/2)*2!=j){
                            System.out.print(cs[j]);
                        }
                    }

                    System.out.println();
            }
    }
}
