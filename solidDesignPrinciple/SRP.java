package solidDesign;

//a class should only have one responsibility
class UserSrp {

	private String name;
	private String email;
	private String address;

	//constructor, getters and setters

	//violate the single responsibility principle
	void printTextToConsole() {

	}
}

//solution of single responsibility principle
class UserPrinter {

	void printTextToConsole(String text) {

	}

	void printTextToAnotherMedium(String text) {

	}
}
