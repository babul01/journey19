package solidDesign;

interface IUserDip{
	public String getName();
}

class UserDip implements IUserDip{

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

}

class AnotherUserDip implements IUserDip{

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}
}

class Main {
	
	private IUserDip userDip;
	
	public Main(IUserDip userDip) {
		this.userDip = userDip;
	}
}

public class DIP {

	public static void main(String[] args) {
		
		IUserDip userDip = new UserDip();
		IUserDip anotherUserDip = new AnotherUserDip();  
		
		Main main1 = new Main(userDip);
		Main main2 = new Main(anotherUserDip);
	}
}