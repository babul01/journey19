package solidDesign;

//Open for Extension, Closed for Modification
class UserOcp {

	private String name;
	private String email;
	private String address;

	// violate the open-closed principle
	private String requestToken;
	private String accessToken;
	private String serviceId;

	// constructor, getters and setters
}

//solution of open-closed principle
class SocialUser extends UserOcp {

	private String requestToken;
	private String accessToken;
	private String serviceId;

	// constructor, getters and setters
}
