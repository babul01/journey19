package solidDesign;

interface IUser {
	public String getName();
	public String getEmail();
}

interface ILogable{
	public String getLog();
}

class UserIsp implements IUser{
	
	private String name;
	private String email;
	private String address;
	
	//constructor, getters and setters
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getEmail() {
		// TODO Auto-generated method stub
		return null;
	}
}

class LoggableUser implements IUser, ILogable{

	@Override
	public String getLog() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getEmail() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
