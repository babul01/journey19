package solidDesign;

class UserLsp {

	private String name;
	private String email;
	private String address;

	// violate the open-closed principle
	private String requestToken;
	private String accessToken;
	private String serviceId;

	// constructor, getters and setters
}

//solution of open-closed principle
class FacebookUser extends UserLsp {

	private String requestToken;
	private String accessToken;
	private String serviceId;

	// constructor, getters and setters
}
