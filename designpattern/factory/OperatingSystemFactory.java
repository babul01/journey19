package designpattern.factory;

public class OperatingSystemFactory {
	
	public OS getInstance(String OSName) {
		return OSName.equals("Android") ? new Android() : OSName.equals("IOS") ? new IOS() : new Windows();
	}
}
