package designpattern.factory;

public class FactoryMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		OperatingSystemFactory osf = new OperatingSystemFactory();
		OS os = osf.getInstance("Windows");
		os.show();
	}

}
