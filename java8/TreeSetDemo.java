package learing.java8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeSet;

public class TreeSetDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TreeSet<Integer> list = new TreeSet<>((i1, i2) -> (i1<i2)?1:(i1>i2)?-1:0);
		
		list.add(10);
		list.add(100);
		list.add(5);
		list.add(12);
		list.add(30);
		list.add(90);
		
		System.out.println("Before sorting "+list);
		
		
		
		//System.out.println("After sorting "+list);
		
	}
}
