package java8;

import java.util.function.*;
import java.util.*;

class Employee{
    
    String name;
    double salary;

    public Employee(String name, double salary){
        
        this.name = name;
        this.salary = salary;
    }

    public String toString(){
        
        return name+" "+salary;
    }
}

public class EmployeeByPredicate{
    
    public static void main(String[] args){
        
        ArrayList<Employees> list = new ArrayList<>();
        populateList(list);

        Predicate<Employees> p = e -> e.salary > 7000;

        for(Employees e : list){

            if(p.test(e)){

                System.out.println(e);
            }
        }
    }

    public static void populateList(ArrayList<Employees> list){

        list.add(new Employees("name1", 5000));
        list.add(new Employees("name2", 6000));
        list.add(new Employees("name3", 7000));
        list.add(new Employees("name4", 8000));
        list.add(new Employees("name5", 9000));
    }
}
