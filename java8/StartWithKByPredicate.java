import java.util.function.*;

public class StartWithKByPredicate{
    
    public static void main(String[] args){

        String[] names = {"Karina","Pori","Champa","Kajol","Katrina"};

        Predicate<String> p = s -> s.charAt(0)=='K';
        
        for(String s : names){
            
            if(p.test(s)){
                
                System.out.println(s);
            }
        }
    }
}
