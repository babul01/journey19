import java.util.*;
import java.util.function.*;

class Employee{
	
	String name;
	double salary;

	public Employee(String name, double salary){
		
		this.name = name;
		this.salary = salary;
	}

	public String toString(){
		
		return this.name +" : "+this.salary;
	}
}

public class SalaryIncrement{
	
	public static void main(String[] args){
			
		ArrayList<Employee> list = new ArrayList<>();
		populate(list);

		Predicate<Employee> p = e -> e.salary < 8000;

		Function<Employee, Employee> f = e -> {
			
			e.salary += 100;
			return e;
		};

		for(Employee e : list){
			
			if(p.test(e)){
				
				f.apply(e);
			}
		}

		System.out.println(list);
	}

	public static void populate(ArrayList<Employee> list){
		
		list.add(new Employee("name1",5000));
		list.add(new Employee("name2",6000));
		list.add(new Employee("name3",7000));
		list.add(new Employee("name4",8000));
		list.add(new Employee("name5",9000));
	}
}
