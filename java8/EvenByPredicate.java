import java.util.function.*;

public class EvenByPredicate{
    
    public static void main(String[] args){

        Predicate<Integer> p = n -> n % 2 == 0;

        System.out.println(p.test(100));
        System.out.println(p.test(101));
    }
}
