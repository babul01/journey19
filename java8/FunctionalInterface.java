package learing.java8;

import java.util.function.Predicate;

interface Interf{
	public int squareIt(int n);
}

public class FunctionalInterface {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Interf  i = n -> n * n;
		
		System.out.println(i.squareIt(4));
		System.out.println(i.squareIt(5));
		
		Predicate<Integer> p = n -> n > 10;
		
		System.out.println(p.test(100));
		System.out.println(p.test(5));
	}

}
