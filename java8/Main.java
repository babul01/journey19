package learing.java8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Integer> list = new ArrayList<>();
		
		list.add(10);
		list.add(100);
		list.add(5);
		list.add(12);
		list.add(30);
		list.add(90);
		
		System.out.println("Before sorting "+list);
		
		Collections.sort(list, new MyComparator());
		
		System.out.println("After sorting "+list);
		
		System.out.println(list.stream().filter(i->i%2==0).collect(Collectors.toList()));
		
		System.out.println(list.stream().map(i->i*2).collect(Collectors.toList()));
		
	}
}
