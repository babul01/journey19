import java.util.function.*;

public class SquareByFunction{

    public static void main(String[] args){
        
        Function<Integer, Integer> f = i -> i * i;

		System.out.println(f.apply(9));
        
    }
    
}
