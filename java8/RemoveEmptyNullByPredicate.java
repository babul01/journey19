import java.util.*;
import java.util.function.*;

public class RemoveEmptyNullByPredicate{
	
	public static void main(String[] args){
		
		ArrayList<String> list1 = new ArrayList<>();
		list1.add("name1");
		list1.add(null);
		list1.add("name2");
		list1.add("");
		list1.add("name2");

		System.out.println(list1);

		Predicate<String> p = s -> s!=null && s.length()!=0;

		ArrayList<String> list2 = new ArrayList<>();

		for(String s : list1){
			
			if(p.test(s)){
				
				list2.add(s);
			}
		}

		System.out.println(list2);
	}
}
