package java8;

import java.util.function.*;
import java.util.*;

class Employees {

    String name;
    double salary;

    public Employees(String name, double salary){

        this.name = name;
        this.salary = salary;
    }
}


public class EmployeeTotalSalaryByFunction{
    
    public static void main(String[] args){

        ArrayList<Employees> l = new ArrayList<>();
        populate(l);
        
        Function<ArrayList<Employees>, Double> f = list -> {
        
        double total = 0;

            for(Employees e : list){

                total += e.salary;
            }
            
            return total;
        };

        System.out.println(f.apply(l));
    }

    public static void populate(ArrayList<Employees> l){

        l.add(new Employees("name1",5000));
        l.add(new Employees("name2",6000));
        l.add(new Employees("name3",7000));
        l.add(new Employees("name4",8000));
        l.add(new Employees("name5",9000));
    }
}
