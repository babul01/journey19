package javabasic;

public class MethodOverriding {
    public static void main(String[] args) {
        Child child = new Child();
        child.property();
        child.marry();
    }
}

class Parent {
    public void property() {
        System.out.println("Cash+Land+Gold");
    }
    //Overridden Method
    public void marry() {
        System.out.println("Bobita");
    }
}

class Child extends Parent{
    //Overriding Method
    public void marry() {
        System.out.println("Purnima");
    }
}
