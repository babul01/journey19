package mathsnippets;

import java.util.Scanner;

public class PrimeNumber2{
	public static void main(String[] args){

		int n, i, ch;
		Scanner in = new Scanner(System.in);

		n = in.nextInt();
		i = 2;
		ch = 0;

		while(i<=n/2){
			if(n%i==0){
				ch = 1;
				break;
			}
			i++;
		}

		if(ch==1){
			System.out.println("Number is not prime");
		}else{
			System.out.println("Number is prime");
		}
	}
}
