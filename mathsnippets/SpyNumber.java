package mathsnippets;

import java.util.Scanner;

public class SpyNumber{

        public static void main(String[] args){

                int n, pro, sum, tmp, r;
                Scanner in = new Scanner(System.in);

		n = in.nextInt();
		tmp = n;
		pro = 1;
		sum = 0;

		while(tmp > 0){

			r = tmp % 10;
			pro *= r;
			sum += r;
			tmp/=10;
		}

		if(pro == sum){
			
			System.out.println("The number is Spy Number");

		}else{
			
			System.out.println("The number is not Spy Number");		
		}

        }
}

