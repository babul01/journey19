package mathsnippets;

import java.util.Scanner;

public class SumDigits{
	
	public static void main(String[] args){
		
		int n, sum;
		Scanner in = new Scanner(System.in);

		n = in.nextInt();
		sum = 0;

		while(n > 0){

			sum += n % 10;
			n /= 10;
		}

		System.out.println(sum);
	}
}
