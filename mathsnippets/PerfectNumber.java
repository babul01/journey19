package mathsnippets;

import java.util.Scanner;

public class PerfectNumber{
	
	public static void main(String[] args){
		
		int n, sum;
		Scanner in = new Scanner(System.in);

		n = in.nextInt();
		sum = 0;

		for(int i = 1; i<n; i++){
			
			if(n % i == 0){
				
				sum+=i;
			}
		}

		if(sum==n){

			System.out.println("The number is perfect");

		}else{
			
			System.out.println("The number is not perfect");
		}
	}
}
