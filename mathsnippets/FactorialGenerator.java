package mathsnippets;

import java.util.Scanner;

public class FactorialGenerator{
	
	public static void main(String[] args){
		
		int n;
		Scanner in = new Scanner(System.in);

		n = in.nextInt();

		for(int i = 1; i<=n; i++){
			
			System.out.println(fact(i));
		}
	
	}

	private static int fact(int n){
		
		int fact = 1;

		for(int i = 1; i<=n; i++){
			
			fact *= i;
		}

		return fact;
	}
}
