package mathsnippets;

import java.util.Scanner;

public class PascalTriangle{

	public static void main(String[] args){
		
		int n;
		Scanner in = new Scanner(System.in);

		n = in.nextInt();

		for(int i = 0; i<n; i++){
			
			for(int j = 0; j<n-i-1; j++){
				
				System.out.print(" ");
			}

			for(int j = 0; j<n; i++){
				
				System.out.print(" "+fact(i) / (fact(j) * fact(i-j)));
			}

			System.out.println();
		}
	}

	private static int fact(int n){
		
		int fact = 1;
		
		for(int i = 1; i<=n; i++){
			
			fact *= i;
		}

		return fact;
	}
}
