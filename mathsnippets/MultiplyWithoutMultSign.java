package mathsnippets;

import java.util.Scanner;

public class MultiplyWithoutMultSign{
	
	public static void main(String[] args){
		
		int n1, n2, mult;
		Scanner in = new Scanner(System.in);

		n1 = in.nextInt();
		n2 = in.nextInt();
		mult = 0;

		if(n2 < 0){
			
			n1 = -n1;
			n2 = -n2;
		}

		for(int i = 0; i<n2; i++){
			
			mult += n1;
		}

		System.out.println(mult);
	}
}
