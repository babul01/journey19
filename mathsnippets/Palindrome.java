package mathsnippets;

import java.util.Scanner;

public class Palindrome{
	
	public static void main(String[] args){
		
		String text, reverseText;
		Scanner in = new Scanner(System.in);

		text = in.nextLine();

		StringBuilder sb = new StringBuilder(text);
		reverseText = sb.reverse().toString();

		if(text.equals(reverseText)){
			
			System.out.println("Your text is palindrome");
		}else{
		
			System.out.println("Your text is not palindrome");
		}
	}
}
