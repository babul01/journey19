package mathsnippets;

import java.util.Scanner;

public class AutomorphicNumber{

        public static void main(String[] args){

                int n, sq, r, i, flag, tmp;
                Scanner in = new Scanner(System.in);

		n = in.nextInt();
		sq = n * n;
		tmp = sq;
		i = 10;
		flag = 0;

		while(tmp > 0){
			
			r = sq % i;

			if(r == n){
				
				flag = 1;	
				break;
			}

			i*=10;
			tmp/=10;
		}

		if(flag == 1){

			System.out.println("The number is Automorphic");
		}else{
			System.out.println("The number is not Automorphic");
		}

        }
}

