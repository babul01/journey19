package mathsnippets;

import java.util.Scanner;

public class NumbersPower{
	
	public static void main(String[] args){
		
		int n, p, r;
		Scanner in = new Scanner(System.in);
		
		n = in.nextInt();
		p = in.nextInt();
		r = 1;

		for(int i = 0; i<p; i++){
			
			r *= n;

		}

		System.out.println(r);
	}
}
