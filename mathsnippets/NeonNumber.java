package mathsnippets;

import java.util.Scanner;

public class NeonNumber{

        public static void main(String[] args){

                int n, sq, sum, r;
                Scanner in = new Scanner(System.in);

		n = in.nextInt();
		sq = n * n;
		sum = 0;

		while(sq > 0){
		
			r = sq % 10;
			sum += r;
			sq/=10;
		}

		if(sum == n){
			
			System.out.println("This is a Neon Number");
		}else{
		
			System.out.println("This is not Neon Number");
		}

        }
}
