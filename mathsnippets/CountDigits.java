package mathsnippets;

import java.util.Scanner;

public class CountDigits{
	
	public static void main(String[] args){
		
		int n, count;
		Scanner in = new Scanner(System.in);

		n = in.nextInt();
		count = 0;

		while(n > 0){
			
			count ++;
			n /= 10;
		}

		System.out.println(count);

	}
}
