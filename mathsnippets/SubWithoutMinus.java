package mathsnippets;

import java.util.Scanner;

public class SubWithoutMinus{
	
	public static void main(String[] args){
		
		int n1, n2, sub;
		Scanner in = new Scanner(System.in);

		n1 = in.nextInt();
		n2 = in.nextInt();

		sub = n1 + (~n2+1);

		System.out.println(sub);
	}
}
