package mathsnippets;

import java.util.Scanner;
import java.util.Arrays;

public class Anagram{
	
	public static void main(String[] args){
		
		String str1, str2;
		Scanner in = new Scanner(System.in);

		str1 = in.nextLine();
		str2 = in.nextLine();

		if(isAnagram(str1, str2)){
			
			System.out.println("These are anagram string");
		}else{
			
			System.out.println("These are not anagram string");
		}
	}

	private static boolean isAnagram(String str1, String str2){
		
		char[] w1 = str1.replaceAll("\\s","").toLowerCase().toCharArray();
		char[] w2 = str2.replaceAll("\\s","").toLowerCase().toCharArray();

		Arrays.sort(w1);
		Arrays.sort(w2);

		return Arrays.equals(w1,w2);
	}
}
