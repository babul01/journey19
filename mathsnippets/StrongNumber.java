package mathsnippets;

import java.util.Scanner;

public class StrongNumber{
	
	public static void main(String[] args){
		
		int n, tmp, i, pro, sum, r;
		Scanner in = new Scanner(System.in);
		
		n = in.nextInt();
		tmp = n;
		sum = 0;

		while(tmp > 0){
			
			i = 1;
			pro = 1;
			r = tmp % 10;

			while(i <= r){
				
				pro *= i;
				i++;	
			}

			sum+=pro;
			tmp/=10;
		}

		if(sum == n){
			
			System.out.println("This is a Strong Number");
		}else{
			
			System.out.println("This is not Strong Number");		
		}
	}
}
