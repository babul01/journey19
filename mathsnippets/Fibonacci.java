package mathsnippets;

import java.util.Scanner;

public class Fibonacci{
	
	public static void main(String[] args){
		
		int n, first, second, result;
		Scanner in = new Scanner(System.in);

		n = in.nextInt();
		first = 0;
		second = 1;
		result = 0;

		while(true){
			
			result = first + second;

			if(result >= n){
				break;
			}

			first = second;
			second = result;

			System.out.println(result);
		}
	}
}
