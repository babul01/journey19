package mathsnippets;

import java.util.Scanner;

public class SwapTwoNumber2{

	public static void main(String[] args){
		
		int n1, n2;
		Scanner in = new Scanner(System.in);
		
		System.out.print("Enter first number: ");
		n1 = in.nextInt();

		System.out.print("Enter second number: ");
		n2 = in.nextInt();
		
		System.out.println("Before swapping: n1 = "+n1+" n2 = "+n2);
		n1 = n1 + n2;
		n2 = n1 - n2;
		n1 = n1 - n2;

		System.out.println("After swapping: n1 = "+n1+" n2 = "+n2);

			
	}
}
