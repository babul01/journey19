package mathsnippets;

import java.util.Scanner;

public class ReverseNumber{
	
	public static void main(String[] args){
		
		int n, tmp, r;
		Scanner in = new Scanner(System.in);

		n = in.nextInt();
		tmp = 0;
		r = 0;

		while(n > 0){
			
			r *= 10;
			r += n % 10;
			n /= 10;
		}

		System.out.println(r);
	}
}
