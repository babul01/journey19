package mathsnippets;

import java.util.Scanner;

public class PrimeNumberGenerator{
	
	public static void main(String[] args){
		
		int n, p;
		Scanner in = new Scanner(System.in);

		n = in.nextInt();
		p = 2;

		for(int i = 0; i < n; i++){
			
			if(isPrime(p)){
				
				System.out.println(p);
			}
			p++;
		}

	}

	public static boolean isPrime(int n){
		
		for(int i = 2; i <= n/2; i++){
			
			if(n % i == 0){
				
				return false;
			}
		}

		return true;
	}
}
