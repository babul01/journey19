package mathsnippets;

import java.util.Scanner;

public class SuperDigit{
	
	public static void main(String[] args){
		
		int n, tmp, sum;
		Scanner in = new Scanner(System.in);

		n = in.nextInt();

		while(n >= 10){
			
			sum = 0;
			tmp = n;

			while(tmp > 0){
				
				sum += tmp % 10;
				tmp/=10;
			}

			n = sum;
		}

		System.out.println("Super digit: "+n);
	}
}
