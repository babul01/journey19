package mathsnippets;

import java.util.Scanner;

public class ModWithoutModSign{
	
	public static void main(String[] args){
		
		int n1, n2, r;
		Scanner in = new Scanner(System.in);

		n1 = in.nextInt();
		n2 = in.nextInt();

		r = n1 / n2;
		r = n1 - n2 * r;

		System.out.println(r);
	}
}
