package mathsnippets;

import java.util.Scanner;

public class EvenOrOdd{

	public static void main(String[] args){
		
		int n;
		Scanner in = new Scanner(System.in);
		
		n = in.nextInt();

		if(n%2==0){
			System.out.println("The number is Even");
		}else{
			System.out.println("The number is Odd");
		}
	}
}
