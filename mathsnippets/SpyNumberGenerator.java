package mathsnippets;

import java.util.Scanner;

public class SpyNumberGenerator{
	
	public static void main(String[] args){
		
		int n;

		Scanner in = new Scanner(System.in);

		n = in.nextInt();
		
		for(int i = 10; i <= n; i++){
			
			if(isSpyNumber(i)){
				
				System.out.println(i);
			}
		}

	}

	private static boolean isSpyNumber(int n){
		
		int pro, sum, r;
		
		pro = 1;
		sum = 0;

		while (n > 0){
			
			r = n % 10;
			pro *= r;
			sum += r;
			n /= 10;
		}

                if(pro == sum){

                        return true;
                }

		return false;
	}
}
