package mathsnippets;

import java.util.Scanner;

public class PrimeNumber{
	public static void main(String[] args){
		
		int n, i, count = 0;
		Scanner in = new Scanner(System.in);

		n = in.nextInt();

		for(i = 1; i<=n; i++){
			if(n%i==0){
				count++;
			}
		}

		if(count==2){
			System.out.println("Number is Prime");
		}else{
			System.out.println("Number is not Prime");
		}
	}
}
