package mathsnippets;

import java.util.Scanner;

public class ArmstrongNumber{
	
	public static void main(String[] args){
		
		int n, r, sum, tmp;
		Scanner in = new Scanner(System.in);

		n = in.nextInt();
		sum = 0;
		tmp = n;

		while(tmp > 0){
			
			r = tmp % 10;
			sum += r*r*r;
			tmp/=10;
		}

		if(sum == n){
			
			System.out.println("The number is armstrong");
		}else{
			
			System.out.println("The number is not armstrong");
		}


	}
}
