package mathsnippets;

import java.util.Scanner;

public class AutomorphicGenerator{

        public static void main(String[] args){

                int n;
                Scanner in = new Scanner(System.in);

		n = in.nextInt();

		for(int i = 5; i<=n; i++){
			
			if(isAutomorphic(i)){
				
				System.out.println(i);
			}
		}

	}

	private static boolean isAutomorphic(int n){
		
		int t, r, tmp;
		int sq = n * n;
		
		t = 10;
		tmp = sq;

		while(tmp > 0){
			
			r = 0;

			r += sq % t;	
			
			if(r == n){
				
				return true;
			}
			
			t *= 10;
			tmp /= 10;

		}

		return false;
	}
}

