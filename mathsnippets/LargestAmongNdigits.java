package mathsnippets;

import java.util.Scanner;

public class LargestAmongNdigits{

	public static void main(String[] args){
		
		int arr[], n, tmp;
		Scanner in = new Scanner(System.in);

		System.out.print("Enter length of Array: ");
		n = in.nextInt();
		arr = new int[n];
		tmp = 0;

		System.out.println("Enter Array Elements: ");
		for(int i = 0; i<n; i++){
			arr[i] = in.nextInt();
		}

		for(int j = 0; j<n; j++){
			if(arr[j] > tmp){
				tmp = arr[j];
			}
		}

		System.out.println("Largest elements of Array: "+tmp);

  }
}
