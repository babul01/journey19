package mathsnippets;

import java.util.Scanner;

public class PrintingNumber{
	
	static int counter = 1;

	public static void main(String[] args){
		
		int n;
		Scanner in = new Scanner(System.in);

		n = in.nextInt();

		printNumber(n);
	}

	private static void printNumber(int n){
		
		System.out.println(counter);
		
		counter++;

		if(counter == n+1){
			
			return;
		}

		printNumber(n);
	}
}
