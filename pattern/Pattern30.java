public class Pattern30{
	
	public static void main(String[] args){
		
		int flag = 1;

		for(int i = 5; i>0; i--){
			
			for(int j = 1; j<=flag; j++){
				
				System.out.print(" ");
			}

			flag++;

			for(int k = 0; k<i; k++){
				
				System.out.print(i);
			}

			System.out.println();
		}
	}
}
