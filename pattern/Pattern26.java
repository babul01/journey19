public class Pattern26{
	
	public static void main(String[] args){
		
		int flag = 4;

		for(int i = 1; i<=5; i++){
			
			for(int j = 0; j<flag; j++){
				
				System.out.print(" ");
			}
			
			flag--;

			for(int k = 1; k<=i; k++){
				
				System.out.print(k);
			}

			System.out.println();
		}
	}
}
