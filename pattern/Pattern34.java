public class Pattern34{
	
	public static void main(String[] args){
		
		int n = 5;
		int star = 1;

		for(int i = n; i>0; i--){
			
			for(int j = 0; j<i; j++){
				
				System.out.print(" ");
			}

			for(int k = 0; k<star; k++){
				
				System.out.print("*");
			}
			
			star+=2;

			System.out.println();
		}
	}
}
