public class Pattern18{
	
	public static void main(String[] args){
		
		int flag = 'E';

		for(int i = 'E'; i>='A'; i--){
			
			for(int j = 'A'; j<=i; j++){
				
				System.out.print((char)flag);
			}

			System.out.println();
			flag--;
		}
	}
}
