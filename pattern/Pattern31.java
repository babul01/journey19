public class Pattern31{
	
	public static void main(String[] args){
		
		int flag = 5;

		for(int i = 0; i<5; i++){
			
			for(int j = 0; j<i; j++){
				
				System.out.print(" ");
			}

			for(int k = 1; k<=flag; k++){
				
				System.out.print(k);
			}
			
			flag--;

			System.out.println();
		}
	}
}
