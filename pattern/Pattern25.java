public class Pattern25{
	
	public static void main(String[] args){
		
		int flag = 4;

		for(int i = 1; i<=5; i++){
			
			for(int k = 0; k<=flag; k++){
				
				System.out.print(" ");
			}
			
			flag--;

			for(int j = 1; j<=i; j++){
				
				System.out.print(i);
			}

			System.out.println();
		}
	}
}
