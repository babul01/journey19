public class Pattern24{
	
	public static void main(String[] args){
		
		int flag = 4;

		for(int i = 0; i<5; i++){

			for(int j = 0; j<flag; j++){
				
				System.out.print(" ");
			}
			
			flag--;

			for(int j = 0; j<=i; j++){
				
				System.out.print("*");
			}

			System.out.println();
		}
	}
}
