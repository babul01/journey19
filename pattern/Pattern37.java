public class Pattern37{
	
	public static void main(String[] args){
		
		int n = 5;
		int lCount = 1;

		for(int i = 0; i<n; i++){
			
			for(int j = n; j>i; j--){
				
				System.out.print(" ");
			}

			for(int k = 0; k<lCount; k++){
				
				System.out.print((char)(i+65));
			}

			lCount+=2;

			System.out.println();
		}
	}
}
