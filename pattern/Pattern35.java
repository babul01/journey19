public class Pattern35{
	
	public static void main(String[] args){
		
		int n = 1;

		for(int i = 1; i<=5; i++){
			
			for(int j = 5; j>i; j--){
				
				System.out.print(" ");
			}

			for(int k = 1; k<=n; k++){
				
				System.out.print(i);
			}

			n+=2;
			System.out.println();
		}
	}
}
