public class Pattern36{
	
	public static void main(String[] args){
		
		int n = 1;

		for(int i = 5; i>0; i--){
			
			for(int j = 0; j<i; j++){
				
				System.out.print(" ");
			}

			for(int k = 0; k<n; k++){
				
				System.out.print(n);
			}

			n+=2;

			System.out.println();
		}
	}
}
