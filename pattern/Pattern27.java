public class Pattern27{
	
	public static void main(String[] args){
	
		int flag = 4;

		for(int i = 'A'; i<='E'; i++){
			
			for(int j = 0; j<flag; j++){
				
				System.out.print(" ");
			}

			flag--;

			for(int k = 'A'; k<=i; k++){
				
				System.out.print((char)i);
			}

			System.out.println();
		}
	}
}
